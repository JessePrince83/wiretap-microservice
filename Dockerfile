# These ARGs statements are to declare default values
ARG NAFIGOS_BUILD_IMAGE="golang:1.16"

FROM ${NAFIGOS_BUILD_IMAGE} as base
ARG NAFIGOS_BUILD_IMAGE
ARG SKAFFOLD_GO_GCFLAGS
COPY ./ /wiretap-microservice/
WORKDIR /wiretap-microservice/
RUN eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"

FROM gcr.io/distroless/base-debian10
ARG GOTRACEBACK
ENV GOTRACEBACK="$GOTRACEBACK"
COPY --from=base /wiretap-microservice/ /
CMD ["/wiretap-microservice"]
