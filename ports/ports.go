package ports

import (
	"context"
	"sync"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(c utils.Config)
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start(context.Context)
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fullfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(dc chan types.WiretapData, wg *sync.WaitGroup)
}

// IncomingEventPort is an example interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	InitChannel(dc chan types.WiretapData, wg *sync.WaitGroup)
}

// PersistentStoragePort is a port to store messages
type PersistentStoragePort interface {
	Port
	Create(d types.WiretapData) error
}
