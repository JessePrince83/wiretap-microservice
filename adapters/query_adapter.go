package adapters

import (
	"context"
	"errors"
	"os"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"

	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// QueryAdapter is an example of a Driver Adapter.
type QueryAdapter struct {
	config        utils.Config
	domainChannel chan types.WiretapData
	waitgroup     *sync.WaitGroup
}

// Init is the init function required per the Port interface
func (q *QueryAdapter) Init(c utils.Config) {
	log.Debug("starting adapters.QueryAdapter.Init()")
	q.config = c
	log.Info("q.config.NatsUrl =", q.config.NatsURL)
}

// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (q *QueryAdapter) Start(ctx context.Context) {
	log.Debug("starting adapters.QueryAdapter.Start()")

	defer q.waitgroup.Done()

	nc, err := nats.Connect(q.config.NatsURL, nats.MaxReconnects(types.DefaultNatsMaxReconnect), nats.ReconnectWait(types.DefaultNatsReconnectWait))
	if err != nil {
		log.Fatal("Cannot connect to nats, ", q.config.NatsURL,
			"(max reconnect = ", types.DefaultNatsMaxReconnect,
			", reconnect wait = ", types.DefaultNatsReconnectWait, "): ", err)
	}
	defer nc.Close()
	log.Debug("adapters.QueryAdapter: connected to nats")

	// Subscribe
	log.Debug("adapters.QueryAdapter: subscribing on subject '", ">", "'")
	sub, err := nc.QueueSubscribeSync(">", q.config.NatsQgroup)
	if err != nil {
		log.Fatal("?adapters.QueryAdapter: Cannot create a queued sync subscription")
	}

	// loop until no other messages
	for {

		// create a new child context
		log.Trace("create a derived context")
		childCtx, cancel := context.WithTimeout(ctx, types.DefaultNatsCoreTimeout)

		// get next message; if error, then assume it's a break
		log.Trace("waiting for next message")
		m, err := sub.NextMsgWithContext(childCtx)
		if errors.Is(err, context.Canceled) {
			cancel() // calling cancel here to remove go vet error
			log.Trace("context canceled")
			break
		} else if m == nil || err != nil { // timeout
			cancel() // don't forget to cancel
			break
		}

		// we don't need the context any longer
		cancel()

		// ignore stan pings
		if strings.Index(m.Subject, "_STAN") == 0 {
			log.Trace("stan pings detected, skipping")
			continue
		}

		log.Trace("received message on subject = ", m.Subject, ", reply = ", m.Reply)
		wdata := types.WiretapData{}
		wdata.Subject = m.Subject
		wdata.Reply = m.Reply
		wdata.Data = utils.NatsToCloudEvent(m)

		if wdata.Data.Data() == nil {
			log.Trace("data was nil, skipping")
			continue
		}

		// write to shared domain channel
		log.Trace("adapters.QueryAdapter: sending domain data")
		select {
		case q.domainChannel <- wdata:
			log.Trace("adapters.QueryAdapter: done sending domain data")
		default:
			log.Trace("adapters.QueryAdapter: no message sent, breaking")
			break
		}
	}

	log.Debug("no longer waiting for messages")
	os.Exit(0)
}

// InitChannel initialize the shared channel with the Domain object
func (q *QueryAdapter) InitChannel(dc chan types.WiretapData, waitgroup *sync.WaitGroup) {
	q.domainChannel = dc
	q.waitgroup = waitgroup
}
