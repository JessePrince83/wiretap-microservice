package adapters

import (
	"context"
	"strconv"
	"sync"
	"testing"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

const (
	NatsEventsTestPort = 31313
	NatsTestClusterID  = "test-cluster-id"
	NatsTestSubject    = "events-test-subject"
)

func TestEventAdapter_Init(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		c utils.Config
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "default",
			fields: fields{
				config:        utils.Config{},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &EventAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				waitgroup:     tt.fields.waitgroup,
			}
			e.Init(tt.args.c)
		})
	}
}

func TestEventAdapter_Start(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "simple message",
			fields: fields{
				config: utils.Config{
					NatsURL:           "nats://localhost:" + strconv.Itoa(NatsEventsTestPort),
					NatsClientID:      types.DefaultNatsClientID,
					NatsQgroup:        types.DefaultNatsQGroup,
					NatsClusterID:     NatsTestClusterID,
					NatsEventsSubject: NatsTestSubject,
					NatsDurableName:   types.DefaultNatsDurableName,
				},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			// cannot currently test since there is not a test nats streaming server
			// e := &EventAdapter{
			// 	config:        tt.fields.config,
			// 	domainChannel: tt.fields.domainChannel,
			// 	waitgroup:     tt.fields.waitgroup,
			// }
			t.Log("this test tests nothing")

			/// e.Start(ctx)
		})
	}
}

func TestEventAdapter_InitChannel(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		dc        chan types.WiretapData
		waitgroup *sync.WaitGroup
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "default",
			fields: fields{
				config:        utils.Config{},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &EventAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				waitgroup:     tt.fields.waitgroup,
			}
			e.InitChannel(tt.args.dc, tt.args.waitgroup)
		})
	}
}
