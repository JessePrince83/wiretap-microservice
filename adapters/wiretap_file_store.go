package adapters

import (
	"bufio"
	"encoding/json"
	"os"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// WiretapFilestore is an empty structure
type WiretapFileStore struct {

	// currentDate stores a string of the current date (in UTC)
	currentDate string

	// logDir directory to store new logs
	logDir string

	// prefix of the log file
	prefix string

	// file handle to current log
	currentFile *os.File

	// writer
	currentWriter bufio.Writer
}

// Init is empty initialization for WiretapFileStore
func (wfs *WiretapFileStore) Init(c utils.Config) {
	log.Debug("WiretapFileStore.Init started")

	// verify or create the filestore directory
	err := os.MkdirAll(c.FileStoreDir, 0777)
	if err != nil {
		log.Fatalln("?ERROR: could not create or modify the directory" + c.FileStoreDir)
	}

	wfs.logDir = path.Clean(c.FileStoreDir)
	wfs.prefix = c.FileStoreLogPrefix

	wfs.checkOrRotateFileLog()
}

func (wfs *WiretapFileStore) checkOrRotateFileLog() {
	log.Trace("WiretapFileStore.checkOrRotateFileLog started")

	// get the current date string, in UTC
	now := time.Now().UTC()
	currentDate := now.Format("2006-01-02")

	// if wfs.currentDate is different, rotate log!
	if wfs.currentDate != currentDate {

		newlog := path.Join(wfs.logDir, wfs.prefix+currentDate+".log")
		log.Debug("WiretapFileStore.checkOrRotateFileLog creating/appending log, " + newlog)

		// first close the old handle
		// TODO: what is the best way to finalize this without  setFinalizer
		if wfs.currentFile != nil {
			wfs.currentWriter.Flush()
			err := wfs.currentFile.Close()
			if err != nil {
				log.Warningln("WiretapFileStore: Could not close file on rotate, " + wfs.currentFile.Name())
			}
		}

		// create a new file, this is fatal if error
		f, err := os.OpenFile(newlog, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
		if err != nil {
			log.Fatal(err)
		}

		wfs.currentDate = currentDate
		wfs.currentFile = f
		wfs.currentWriter = *bufio.NewWriter(f)
	}
}

// Create outputs the message into
func (wfs *WiretapFileStore) Create(data types.WiretapData) error {
	log.Trace("WiretapLogStore.Create started")

	logEvent(log.StandardLogger(), data)

	b := json.RawMessage(data.Data.Data())
	if b == nil {
		log.Warn("WiretapFileStore.Create: CloudEvent data could not be retrieved")
		// for now, don't print error, just print nil
	} else {

		var r map[string]interface{}
		err := json.Unmarshal(b, &r)
		if err == nil {

			tm := types.TracedMessage{
				Subject:          data.Subject,
				CloudEventID:     data.Data.Context.GetID(),
				CloudEventSource: data.Data.Context.GetSource(),
				CloudEventType:   data.Data.Context.GetType(),
				TimeStamp:        data.Data.Context.GetTime().String(),
				Payload:          b,
			}
			if actor, ok := r["actor"]; ok {
				actorStr, ok := actor.(string)
				if ok {
					tm.Actor = actorStr
				}
			}
			if emulator, ok := r["emulator"]; ok {
				emulatorStr, ok := emulator.(string)
				if ok {
					tm.Emulator = emulatorStr
				}
			}
			if tid, ok := r["transaction"]; ok {
				tidStr, ok := tid.(string)
				if ok {
					tm.TransactionID = tidStr
				}
			}

			log.Tracef("%+v\n", tm)
			wfs.checkOrRotateFileLog()
			json.NewEncoder(&wfs.currentWriter).Encode(tm)
			wfs.currentWriter.Flush()
		} else {
			log.Warn("WiretapFileStore.Create: Could not unmarshal cloud event data")
		}
	}

	return nil
}

func logEvent(logger *log.Logger, data types.WiretapData) {
	mediaType, _ := data.Data.Context.GetDataMediaType()
	logger.Printf("Message: nats subject=%s nats reply=%s, cloud event metadata={id=%s, source=%s, specversion=%s, type=%s, datacontenttype=%s, dataschema=%s, subject=%s, data-media-type=%s, time=%s}\n",
		data.Subject, data.Reply, data.Data.Context.GetID(), data.Data.Context.GetSource(),
		data.Data.Context.GetSpecVersion(), data.Data.Context.GetType(), data.Data.Context.GetDataContentType(),
		data.Data.Context.GetDataSchema(), data.Data.Context.GetSubject(),
		mediaType, data.Data.Context.GetTime())
}
