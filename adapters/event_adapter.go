package adapters

import (
	"context"
	"os"
	"os/signal"
	"sync"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// EventAdapter is an example of a Driver Adapter. Specifically, this is supposed to be a source of information and
// needs to be able to call functions in the Domain.
type EventAdapter struct {
	config        utils.Config
	domainChannel chan types.WiretapData
	waitgroup     *sync.WaitGroup
}

// Init is the init function required per the Port interface
func (e *EventAdapter) Init(c utils.Config) {
	log.Debug("starting adapters.EventAdapter.Init()")
	e.config = c
	log.Info("e.config.NatsUrl = ", e.config.NatsURL, "e.config.NatsClusterID = ", e.config.NatsClusterID)
}

// Start function tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (e *EventAdapter) Start(ctx context.Context) {
	log.Debug("starting adapters.EventAdapter.Start()")

	defer e.waitgroup.Done()

	nc, err := nats.Connect(e.config.NatsURL, nats.MaxReconnects(types.DefaultNatsMaxReconnect), nats.ReconnectWait(types.DefaultNatsReconnectWait))
	if err != nil {
		log.Fatal("Cannot connect to nats, ", e.config.NatsURL,
			"(max reconnect = ", types.DefaultNatsMaxReconnect,
			", reconnect wait = ", types.DefaultNatsReconnectWait, "): ", err)
	}
	defer nc.Close()
	log.Debug("adapters.EventAdapter: connected to nats")

	sc, err := stan.Connect(e.config.NatsClusterID, e.config.NatsClientID, stan.NatsConn(nc))
	if err != nil {
		log.Fatal("Cannot connect to nats streaming, ", e.config.NatsClusterID, ":", err)
	}
	defer sc.Close()
	log.Debug("adapters.EventAdapter: connected to nats streaming, cluster id = " + e.config.NatsClusterID +
		", client id = " + e.config.NatsClientID)

	// Subscribe
	log.Debug("adapters.EventAdapter: subscribing on subject '", e.config.NatsEventsSubject, "'")
	_, err = sc.QueueSubscribe(e.config.NatsEventsSubject, e.config.NatsQgroup, func(m *stan.Msg) {

		m.Ack()

		log.Trace("adapters.EventAdapter: received message on subject = ", m.Subject, ", reply = ", m.Reply)
		wdata := types.WiretapData{}
		wdata.Subject = m.Subject
		wdata.Reply = m.Reply
		wdata.Sequence = m.Sequence
		wdata.Timestamp = m.Timestamp
		wdata.Data = utils.StanToCloudEvent(m)
		log.Trace("adapters.EventAdapter: m.data = " + string(m.Data))

		// write to shared domain channel
		log.Trace("adapters.EventAdapter: sending domain data")
		select {
		case e.domainChannel <- wdata:
			log.Trace("adapters.EventAdapter: done sending domain data")
		default:
			log.Trace("adapters.EventAdapter: no message sent, breaking")
			break
		}

	}, stan.StartWithLastReceived(), stan.DurableName(e.config.NatsDurableName))
	if err != nil {
		log.Fatal("adapters.EventAdapter: Cannot create a queued  subscription")
	}

	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			cleanupDone <- true
		}
	}()
	<-cleanupDone

	log.Debug("no longer waiting for events")
	os.Exit(0)
}

// InitChannel initialize the shared channel with the Domain object
func (e *EventAdapter) InitChannel(dc chan types.WiretapData, waitgroup *sync.WaitGroup) {
	e.domainChannel = dc
	e.waitgroup = waitgroup
}
