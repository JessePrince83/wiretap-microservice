package adapters

import (
	"context"
	"strconv"
	"sync"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

const (
	NatsQueryTestPort = 31313
)

func TestQueryAdapter_Init(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		c utils.Config
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "default",
			fields: fields{
				config:        utils.Config{},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := &QueryAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				waitgroup:     tt.fields.waitgroup,
			}
			q.Init(tt.args.c)
		})
	}
}

func TestQueryAdapter_Start(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
		doCtxCancel   bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "simple message",
			fields: fields{
				config: utils.Config{
					NatsURL:      "nats://localhost:" + strconv.Itoa(NatsQueryTestPort),
					NatsClientID: types.DefaultNatsClientID,
					NatsQgroup:   types.DefaultNatsQGroup,
				},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
				doCtxCancel:   false,
			},
		},
		{
			name: "context cancel",
			fields: fields{
				config: utils.Config{
					NatsURL:      "nats://localhost:" + strconv.Itoa(NatsQueryTestPort),
					NatsClientID: types.DefaultNatsClientID,
					NatsQgroup:   types.DefaultNatsQGroup,
				},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
				doCtxCancel:   true,
			},
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {

			// create a new context
			ctx, cancel := context.WithCancel(context.Background())

			t.Log("starting test nats server")
			server := utils.RunNatsTestServer(NatsQueryTestPort)
			defer server.Shutdown()

			q := &QueryAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				waitgroup:     tt.fields.waitgroup,
			}

			// increment the waitgroup
			tt.fields.waitgroup.Add(1)

			t.Log("go q.start()")
			go q.Start(ctx)

			// sleep
			t.Log("sleeping")
			time.Sleep(2 * time.Second)

			// check if we should test cancel
			if !tt.fields.doCtxCancel {

				t.Log("send cloud event")
				nc, _ := nats.Connect(tt.fields.config.NatsURL)

				event := cloudevents.NewEvent()
				event.SetID("xxx")
				event.SetType("test")
				event.SetTime(time.Now())
				event.SetSource("TestQueryAdapter_Start")
				event.SetDataContentType("application/json;charset=utf-8")
				event.SetData(cloudevents.TextPlain, "test message")
				j, _ := event.MarshalJSON()
				nc.Publish("cyverse.test", j)
				nc.Flush()
				nc.Close()
				t.Log("done sending cloud event")
			}

			// close the domain channel and then wait
			cancel()
			t.Log("waiting for adapter to close")
			tt.fields.waitgroup.Wait()
			t.Log("closing domainChannel")
			close(tt.fields.domainChannel)
			server.Shutdown()
		})
	}
}

func TestQueryAdapter_InitChannel(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		dc        chan types.WiretapData
		waitgroup *sync.WaitGroup
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "default",
			fields: fields{
				config:        utils.Config{},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := &QueryAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				waitgroup:     tt.fields.waitgroup,
			}
			q.InitChannel(tt.args.dc, tt.args.waitgroup)
		})
	}
}
