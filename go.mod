module gitlab.com/cyverse/wiretap-microservice

go 1.16

require (
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/googleapis/gax-go v1.0.3 // indirect
	github.com/gordonklaus/ineffassign v0.0.0-20210225214923-2e10b2664254 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats-server/v2 v2.1.9
	github.com/nats-io/nats-streaming-server v0.20.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/stan.go v0.8.2
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/lint v0.0.0-20201208152925-83fdc39ff7b5 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc
)
