package main

import (
	"context"

	log "github.com/sirupsen/logrus"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/wiretap-microservice/adapters"
	"gitlab.com/cyverse/wiretap-microservice/domain"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

/*
This is a template for the base microservice that uses NATS streaming and NATS core.

This example is based on two incoming adapters and one outgoing adapter to mock some
sort of storage system.


*/

func main() {

	// TODO: make this configurable
	log.SetReportCaller(false)

	log.Debug("main: setting up cancelable context")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var config utils.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()

	log.Info("nats url =", config.NatsURL)

	// create an initial Domain object
	var dmain domain.Domain

	// add and initialize the query adapter
	dmain.QueryIn = &adapters.QueryAdapter{}
	dmain.QueryIn.Init(config)

	// add and initialize the events adapter
	dmain.EventsIn = &adapters.EventAdapter{}
	dmain.EventsIn.Init(config)

	dmain.MessageStore = &adapters.WiretapFileStore{}
	dmain.MessageStore.Init(config)

	dmain.Init(config)
	dmain.Start(ctx)
}
