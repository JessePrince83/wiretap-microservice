package utils

import "testing"

func TestConfig_ProcessDefaults(t *testing.T) {
	type fields struct {
		NatsURL           string
		NatsClientID      string
		NatsQgroup        string
		NatsClusterID     string
		NatsDurableName   string
		NatsEventsSubject string
		NatsCoreSubject   string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name:   "default",
			fields: fields{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				NatsURL:           tt.fields.NatsURL,
				NatsClientID:      tt.fields.NatsClientID,
				NatsQgroup:        tt.fields.NatsQgroup,
				NatsClusterID:     tt.fields.NatsClusterID,
				NatsDurableName:   tt.fields.NatsDurableName,
				NatsEventsSubject: tt.fields.NatsEventsSubject,
				NatsCoreSubject:   tt.fields.NatsCoreSubject,
			}
			c.ProcessDefaults()
		})
	}
}
