package utils

import (
	"encoding/json"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"

	natsserver "github.com/nats-io/nats-server/v2/test"
)

// NatsToCloudEvent converts a nats msg to cloud event
func NatsToCloudEvent(msg *nats.Msg) cloudevents.Event {

	event := cloudevents.NewEvent()
	_ = json.Unmarshal(msg.Data, &event)
	return event
}

// StanToCloudEvent converts a stan msg to cloud event
func StanToCloudEvent(msg *stan.Msg) cloudevents.Event {

	event := cloudevents.NewEvent()
	_ = json.Unmarshal(msg.Data, &event)
	return event
}

// NewCloudEventDataString generates a simple CloudEvent with the givent content string
func NewCloudEventDataString(content string) cloudevents.Event {

	event := cloudevents.NewEvent()
	event.SetID(xid.New().String())
	event.SetSource("cyverse.mock")
	event.SetType("string")
	event.SetDataContentType("text/plain;charset=utf-8")
	event.SetDataSchema("")
	event.SetSubject("cyverse.generated.mock")
	event.SetTime(time.Now())
	event.SetData(cloudevents.TextPlain, content)

	return event
}

// NewCloudEventDataJSON generates a simple CloudEvent with the givent content string
func NewCloudEventDataJSON(content []byte) cloudevents.Event {

	event := cloudevents.NewEvent()
	event.SetID(xid.New().String())
	event.SetSource("cyverse.mock")
	event.SetType("string")
	event.SetDataContentType("application/json;charset=utf-8")
	event.SetDataSchema("")
	event.SetSubject("cyverse.generated.mock")
	event.SetTime(time.Now())
	event.SetData(cloudevents.TextPlain, content)

	return event
}

// These functions are useful for testing

// RunNatsTestServer starts a nats test server on port
// to use:
// 		s := RunNatsTestServer(9999)
//		defer s.Shutdown()
//
//      nc, err := nats.Connect("http://localhost:999")
//		defer nc.Close()
//
//      // do nats stuff here
//
func RunNatsTestServer(port int) *server.Server {
	opts := natsserver.DefaultTestOptions
	opts.Port = port
	return natsserver.RunServer(&opts)
}
