#!/bin/bash

TAG="stack0verflow/wiretap:latest"

docker build -t $TAG .
if [ $? -eq 0 ]; then
  docker push $TAG
  echo "NOTICE: image build and push to dockerhub was successful"
fi
