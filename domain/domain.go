package domain

import (
	"context"
	"sync"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/wiretap-microservice/ports"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn      ports.IncomingQueryPort
	EventsIn     ports.IncomingEventPort
	MessageStore ports.PersistentStoragePort
}

// Init initializes all the specified adapters
func (d Domain) Init(c utils.Config) {
	log.Debug("domain.Init() starting")
}

// Start will start the domain object, and in turn start all the async adapters
func (d Domain) Start(ctx context.Context) {
	log.Debug("domain.Start() starting")

	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	log.Trace("domain.Start: starting QueryIn.Start")
	dc := make(chan types.WiretapData, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(dc, &wg)
	wg.Add(1)
	go d.QueryIn.Start(ctx)

	log.Trace("domain.Start: starting EventsIn.Start")
	d.EventsIn.InitChannel(dc, &wg)
	wg.Add(1)
	go d.EventsIn.Start(ctx)

	// start the domain's query worker
	wg.Add(1)
	log.Trace("domain.Start: processWorker")
	go d.processWorker(ctx, dc, &wg)

	wg.Wait()
}

func (d Domain) processWorker(ctx context.Context, dc chan types.WiretapData, wg *sync.WaitGroup) {
	log.Debug("domain.processQueryWorker starting")
	defer wg.Done()

	for {
		select {
		case data := <-dc:
			log.Trace("domain.processWorker received new data")
			d.MessageStore.Create(data)
		case <-ctx.Done():
			log.Trace("\tdomain.processWorker context cancellation")
			break
		}
	}
}
